/*
    Copyright (C) 2012/2013 Joan Bruguera Micó.

    This file is part of MusCaldron.

    MusCaldron is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MusCaldron is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MusCaldron.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "hand.h"
#include "exhaustive.h"
#include <sstream>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(&watcher, SIGNAL(progressValueChanged(int)), SLOT(onEvaluationProgressChange(int)));
    connect(&watcher, SIGNAL(finished()), SLOT(onEvaluationFinished()));

    setGuiControlState(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setGuiControlState(bool evaluating)
{
    ui->startButton->setEnabled(!evaluating);
    ui->stopButton->setEnabled(evaluating);
    ui->progressBar->setEnabled(evaluating);
}

void MainWindow::on_startButton_clicked()
{
    try
    {
        QCheckBox *has2K[] = { ui->p2Has2K, ui->p3Has2K, ui->p4Has2K };
        QCheckBox *has2A[] = { ui->p2Has2A, ui->p3Has2A, ui->p4Has2A };
        QCheckBox *has3K[] = { ui->p2Has3K, ui->p3Has3K, ui->p4Has3K };
        QCheckBox *has3A[] = { ui->p2Has3A, ui->p3Has3A, ui->p4Has3A };
        QCheckBox *hasTrips[] = { ui->p2HasTrips, ui->p3HasTrips, ui->p4HasTrips };
        QCheckBox *hasTwoPair[] = { ui->p2HasTwoPair, ui->p3HasTwoPair, ui->p4HasTwoPair };
        QCheckBox *hasNothing[] = { ui->p2HasNothing, ui->p3HasNothing, ui->p4HasNothing };
        QCheckBox *sums30[] = { ui->p2Sums30, ui->p3Sums30, ui->p4Sums30 };
        QCheckBox *sums31[] = { ui->p2Sums31, ui->p3Sums31, ui->p4Sums31 };

        const Hand *p1Hand = Hand::fromString(ui->p1Hand->text().toStdString());

        HandFacts p234Facts[3];
        for (int i = 0; i < 3; i++)
        {
            p234Facts[i].has2K = has2K[i]->isChecked();
            p234Facts[i].has2A = has2A[i]->isChecked();
            p234Facts[i].has3K = has3K[i]->isChecked();
            p234Facts[i].has3A = has3A[i]->isChecked();
            p234Facts[i].hasTrips = hasTrips[i]->isChecked();
            p234Facts[i].hasTwoPair = hasTwoPair[i]->isChecked();
            p234Facts[i].hasNothing = hasNothing[i]->isChecked();
            p234Facts[i].sums30 = sums30[i]->isChecked();
            p234Facts[i].sums31 = sums31[i]->isChecked();
        }

        esim.reset(new ExhaustiveSimulation(p1Hand, p234Facts));
        future = esim->start();
        watcher.setFuture(future);
        ui->progressBar->setMinimum(future.progressMinimum());
        ui->progressBar->setMaximum(future.progressMaximum());

        setGuiControlState(true);
    }
    catch (const std::exception &ex)
    {
        QMessageBox::critical(this, tr("Error"), ex.what());
    }
}

void MainWindow::on_stopButton_clicked()
{
    future.cancel();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Q_UNUSED(event);
    
    // Cancel the current evaluation, so the application will exit immediately
    if (future.isRunning())
        future.cancel();
}

void MainWindow::onEvaluationProgressChange(int progressValue)
{
    ui->progressBar->setValue(progressValue);
}

void setBackgroundColor(QLineEdit *edit, double equity)
{
    QColor color;
    if (equity < 0.5)
        color = QColor(0xFF, equity * 2 * 0xFF, 0x00);
    else
        color = QColor(0xFF - (equity - 0.5) * 2 * 0xFF, 0xFF, 0x00);
    QPalette palette;
    palette.setColor(edit->backgroundRole(), color);
    edit->setPalette(palette);
}

void putResults(QLineEdit *playsEdit, QLineEdit *winsEdit, QLineEdit *lossesEdit,
                QLineEdit *drawsEdit, QLineEdit *equityEdit,
                uint64_t plays, uint64_t wins, uint64_t losses)
{
    uint64_t draws = plays - wins - losses;

    playsEdit->setText(QString::number(plays));
    winsEdit->setText(QString::number(wins));
    lossesEdit->setText(QString::number(losses));
    drawsEdit->setText(QString::number(draws));

    if (plays != 0)
    {
        /* A win gets the full pot, a draw half and a lose nothing.
         * So equity = (Wins+Draws/2)/Plays */
        double eq = ((double)wins + (double)draws / 2) / (double)plays;

        equityEdit->setText(QString("%1%").arg(eq * 100.0));
        setBackgroundColor(equityEdit, eq);
    }
    else
    {
        equityEdit->setText(QString("N/A"));
        setBackgroundColor(equityEdit, 0.5);
    }
}

void MainWindow::onEvaluationFinished()
{
    if (!future.isCanceled())
    {
        HandEvaluation eq = future.result();
        putResults(ui->grandePlays, ui->grandeWins, ui->grandeLosses, ui->grandeDraws, ui->grandeEquity,
                   eq.gamesPlayed, eq.grandeWins, eq.grandeLosses);
        putResults(ui->chicaPlays, ui->chicaWins, ui->chicaLosses, ui->chicaDraws, ui->chicaEquity,
                   eq.gamesPlayed, eq.chicaWins, eq.chicaLosses);
        putResults(ui->paresPlays, ui->paresWins, ui->paresLosses, ui->paresDraws, ui->paresEquity,
                   eq.paresPlayed, eq.paresWins, eq.paresLosses);
        putResults(ui->juegoPlays, ui->juegoWins, ui->juegoLosses, ui->juegoDraws, ui->juegoEquity,
                   eq.juegoPlayed, eq.juegoWins, eq.juegoLosses);
        putResults(ui->puntoPlays, ui->puntoWins, ui->puntoLosses, ui->puntoDraws, ui->puntoEquity,
                   eq.puntoPlayed, eq.puntoWins, eq.puntoLosses);
    }

    esim.release();
    setGuiControlState(false);
}
