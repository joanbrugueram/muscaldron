/*
    Copyright (C) 2012/2013 Joan Bruguera Micó.

    This file is part of MusCaldron.

    MusCaldron is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MusCaldron is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MusCaldron.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "hand.h"
#include <stdexcept>
#include <vector>
#include <algorithm>

int cardSymbolToValue(char symbol)
{
    switch (symbol)
    {
    case '1':
    case '2': return 0;
    case '4': return 1;
    case '5': return 2;
    case '6': return 3;
    case '7': return 4;
    case '8': return 5;
    case '9': return 6;
    case 'S': return 7;
    case 'C': return 8;
    case '3':
    case 'R': return 9;
    default: throw std::runtime_error("Invalid card symbol.");
    }
}

char cardValueToSymbol(int value)
{
    switch (value)
    {
    case 0: return '1';
    case 1: return '4';
    case 2: return '5';
    case 3: return '6';
    case 4: return '7';
    case 5: return '8';
    case 6: return '9';
    case 7: return 'S';
    case 8: return 'C';
    case 9: return 'R';
    default: throw std::runtime_error("Invalid card value.");
    }
}

void handStringToValue(int hand[4], const std::string &str)
{
    if (str.length() != 4)
        throw std::runtime_error("Invalid hand length.");

    for (int i = 0; i < 4; i++)
        hand[i] = cardSymbolToValue(str[i]);
}

std::string handValueToString(const int hand[4])
{
    std::string str(4, '?');

    for (int i = 0; i < 4; i++)
        str[i] = cardValueToSymbol(hand[i]);

    return str;
}

static int computeEquivalenceClass(const int cards[4])
{
    // http://en.wikipedia.org/wiki/Multinomial_theorem#Number_of_unique_permutations_of_words
    const int factorials[] = { 1, 1, 2, 6, 24 };
    int equivalence_class = factorials[4];
    for (int i = 0; i < 4;)
    {
        int unique = 1;
        while (i+unique < 4 && cards[i] == cards[i+unique])
            unique++;
        equivalence_class /= factorials[unique];
        i += unique;
    }
    return equivalence_class;
}

static int computeGrandeValue(const int cards[4]) // sorted lower to bigger
{
    int grandeValue = 0;
    for (int i = 3; i >= 0; i--)
        grandeValue = grandeValue * 10 + cards[i];
    return grandeValue;
}

static int computeChicaValue(const int cards[4]) // sorted bigger to lower
{
    int chicaValue = 0;
    for (int i = 0; i < 4; i++)
        chicaValue = chicaValue * 10 + cards[i];
    return -chicaValue; // note the minus sign, so comparisons work!
}

static bool isPair(const int cards[4], int pos)
{
    return cards[pos] == cards[pos+1];
}

static bool isTrips(const int cards[4], int pos)
{
    return cards[pos] == cards[pos+1] && cards[pos+1] == cards[pos+2];
}

static int computeParesValue(const int cards[4])
{
    /* We'll return a number with the following digits in decimal:
     * HLTP
     * Where H is the value of the highest pair of a twopair, or zero.
     * Where L is the value of the lowest pair of a twopair, or zero.
     * Where T is the value of trips, or zero.
     * Where L is the value of a single pair, or zero.
     * (Note that quads is a subset of two pair, no special treatment required).
     *
     * This format makes comparison easy,
     * e.g. if p1 has quads and p2 has trips, then p1Value > p2Value.
     * e.g. if p1 has pair of 3 and p2 has pair of 5, then p2Value > p1Value.
     */
    if (isPair(cards, 0) && isPair(cards, 2)) // Two pair XXYY
        return cards[0] * 1000 + cards[2] * 100;
    else if (isTrips(cards, 0) || isTrips(cards, 1)) // Trips XXX? or ?XXX
        return cards[1] * 10;
    else if (isPair(cards, 0) || isPair(cards, 1)) // Pair XX?? and ?XX?
        return cards[1] * 1;
    else if (isPair(cards, 2)) // Pair ??XX
        return cards[2] * 1;

    return 0;
}

static int computeJuegoSum(const int cards[4])
{
    // Compute non-standarized juego sum
    const int values[] = { 1,4,5,6,7,8,9,10,10,10 };
    int sum = 0;
    for (int i = 0; i < 4; i++)
        sum += values[cards[i]];
    return sum;
}

static int computeJuegoValue(int juegoSum)
{
    // Standarize values over 31
    switch (juegoSum)
    {
        case 31: return 40; // This is the best juego value
        case 32: return 39; // 2nd best
        case 40: return 38; // Etc.
        case 39: return 37;
        case 38: return 36;
        case 37: return 35;
        case 36: return 34;
        case 35: return 33;
        case 34: return 32;
        case 33: return 31; // Worst
    }

    // Sum is under 31 (punto), which has no weird rating rules
    return juegoSum;
}

Hand::Hand(const int cards[4])
{
    std::copy(cards, cards + 4, this->cards);
    std::sort(this->cards, this->cards + 4);

    this->equivalenceClass = computeEquivalenceClass(this->cards);

    this->grandeValue = computeGrandeValue(this->cards);
    this->chicaValue = computeChicaValue(this->cards);
    this->paresValue = computeParesValue(this->cards);
    this->juegoSum = computeJuegoSum(this->cards);
    this->juegoValue = computeJuegoValue(this->juegoSum);

    int kingCount = std::count(this->cards, this->cards + 4, CardValue_King);
    int aceCount = std::count(this->cards, this->cards + 4, CardValue_Ace);
    bool p1 = isPair(this->cards, 0);
    bool p2 = isPair(this->cards, 1);
    bool p3 = isPair(this->cards, 2);
    bool t1 = isTrips(this->cards, 0);
    bool t2 = isTrips(this->cards, 1);
    this->facts.has2K = (kingCount == 2);
    this->facts.has2A = (aceCount == 2);
    this->facts.has3K = (kingCount == 3);
    this->facts.has3A = (aceCount == 3);
    this->facts.hasTrips = (t1 || t2);
    this->facts.hasTwoPair = (p1 && p3);
    this->facts.hasNothing = (!p1 && !p2 && !p3 && this->juegoSum < 31);
    this->facts.sums30 = (this->juegoSum == 30);
    this->facts.sums31 = (this->juegoSum == 31);
}

const Hand *Hand::getHand(const int cards[4])
{
    static struct PrecomputedHands
    {
        std::vector<Hand> precomp;

        PrecomputedHands()
        {
            int cards[4];

            for (cards[0] = 0; cards[0] < 10; cards[0]++)
                for (cards[1] = 0; cards[1] < 10; cards[1]++)
                    for (cards[2] = 0; cards[2] < 10; cards[2]++)
                        for (cards[3] = 0; cards[3] < 10; cards[3]++)
                            precomp.push_back(Hand(cards));
        }
    } precomputedHands;

    int handId = cards[0]+cards[1]*10+cards[2]*100+cards[3]*1000;
    return &precomputedHands.precomp[handId];
}

const Hand *Hand::fromString(const std::string &cardsString)
{
    int cards[4];
    handStringToValue(cards, cardsString);
    return Hand::getHand(cards);
}

std::string Hand::toString(void) const
{
    return handValueToString(this->cards);
}

bool Hand::compliesWith(const HandFacts &requisites) const
{
    if ((requisites.has2K      && !facts.has2K) ||
        (requisites.has2A      && !facts.has2A) ||
        (requisites.has3K      && !facts.has3K) ||
        (requisites.has3A      && !facts.has3A) ||
        (requisites.hasTrips   && !facts.hasTrips) ||
        (requisites.hasTwoPair && !facts.hasTwoPair) ||
        (requisites.hasNothing && !facts.hasNothing) ||
        (requisites.sums30     && !facts.sums30) ||
        (requisites.sums31     && !facts.sums31))
    {
        return false;
    }

    return true;
}

int cmp(int a, int b)
{
    if (a > b) return +1;
    if (a < b) return -1;
    return 0;
}

static int grandeShowdown(const Hand *hands[4])
{
    int team1Best = (hands[0]->grandeValue > hands[1]->grandeValue) ? 0 : 1;
    int team2Best = (hands[2]->grandeValue > hands[3]->grandeValue) ? 2 : 3;
    return cmp(hands[team1Best]->grandeValue, hands[team2Best]->grandeValue);
}

static int chicaShowdown(const Hand *hands[4])
{
    int team1Best = (hands[0]->chicaValue > hands[1]->chicaValue) ? 0 : 1;
    int team2Best = (hands[2]->chicaValue > hands[3]->chicaValue) ? 2 : 3;
    return cmp(hands[team1Best]->chicaValue, hands[team2Best]->chicaValue);
}

static bool isParesPlayed(const Hand *hands[4])
{
    // TODO is pares always played?
    return true;
}

static int paresShowdown(const Hand *hands[4])
{
    int team1Best = (hands[0]->paresValue > hands[1]->paresValue) ? 0 : 1;
    int team2Best = (hands[2]->paresValue > hands[3]->paresValue) ? 2 : 3;
    return cmp(hands[team1Best]->paresValue, hands[team2Best]->paresValue);
}

static bool isJuegoPlayed(const Hand *hands[4])
{
    // TODO is this right?
    if ((hands[0]->juegoValue >= 31 || hands[1]->juegoValue >= 31) &&
        (hands[2]->juegoValue >= 31 || hands[3]->juegoValue >= 31))
        return true;

    return false;
}

static int juegoShowdown(const Hand *hands[4])
{
    int team1Best = (hands[0]->juegoValue > hands[1]->juegoValue) ? 0 : 1;
    int team2Best = (hands[2]->juegoValue > hands[3]->juegoValue) ? 2 : 3;
    return cmp(hands[team1Best]->juegoValue, hands[team2Best]->juegoValue);
}

static bool isPuntoPlayed(const Hand *hands[4])
{
    if (hands[0]->juegoValue < 31 && hands[1]->juegoValue < 31 &&
        hands[2]->juegoValue < 31 && hands[3]->juegoValue < 31)
        return true;

    return false;
}

static int puntoShowdown(const Hand *hands[4])
{
    int team1Best = (hands[0]->juegoValue > hands[1]->juegoValue) ? 0 : 1;
    int team2Best = (hands[2]->juegoValue > hands[3]->juegoValue) ? 2 : 3;
    return cmp(hands[team1Best]->juegoValue, hands[team2Best]->juegoValue);
}

HandEvaluation::HandEvaluation()
{
    gamesSimulated = 0;

    gamesPlayed = 0;
    grandeWins = 0;
    grandeLosses = 0;
    chicaWins = 0;
    chicaLosses = 0;

    paresPlayed = 0;
    paresWins = 0;
    paresLosses = 0;

    juegoPlayed = 0;
    juegoWins = 0;
    juegoLosses = 0;

    puntoPlayed = 0;
    puntoWins = 0;
    puntoLosses = 0;
}

void HandEvaluation::addHandShowdown(const Hand *hands[4])
{
    gamesSimulated++;
    gamesPlayed++;

    // GRANDE
    switch (grandeShowdown(hands))
    {
        case +1: grandeWins++; break;
        case -1: grandeLosses++; break;
    }

    // CHICA
    switch (chicaShowdown(hands))
    {
        case +1: chicaWins++; break;
        case -1: chicaLosses++; break;
    }

    // PARES
    if (isParesPlayed(hands))
    {
        paresPlayed++;
        switch (paresShowdown(hands))
        {
            case +1: paresWins++; break;
            case -1: paresLosses++; break;
        }
    }

    // JUEGO / PUNTO
    if (isJuegoPlayed(hands))
    {
        juegoPlayed++;
        switch (juegoShowdown(hands))
        {
            case +1: juegoWins++; break;
            case -1: juegoLosses++; break;
        }
    }
    else if (isPuntoPlayed(hands))
    {
        puntoPlayed++;
        switch (puntoShowdown(hands))
        {
            case +1: puntoWins++; break;
            case -1: puntoLosses++; break;
        }
    }
}

HandEvaluation& HandEvaluation::operator +=(const HandEvaluation &other)
{
    gamesSimulated += other.gamesSimulated;

    gamesPlayed += other.gamesPlayed;
    grandeWins += other.grandeWins;
    grandeLosses += other.grandeLosses;
    chicaWins += other.chicaWins;
    chicaLosses += other.chicaLosses;

    paresPlayed += other.paresPlayed;
    paresWins += other.paresWins;
    paresLosses += other.paresLosses;

    juegoPlayed += other.juegoPlayed;
    juegoWins += other.juegoWins;
    juegoLosses += other.juegoLosses;

    puntoPlayed += other.puntoPlayed;
    puntoWins += other.puntoWins;
    puntoLosses += other.puntoLosses;

    return *this;
}

HandEvaluation& HandEvaluation::operator *=(const uint64_t factor)
{
    // gamesSimulated stays the same

    gamesPlayed *= factor;
    grandeWins *= factor;
    grandeLosses *= factor;
    chicaWins *= factor;
    chicaLosses *= factor;

    paresPlayed *= factor;
    paresWins *= factor;
    paresLosses *= factor;

    juegoPlayed *= factor;
    juegoWins *= factor;
    juegoLosses *= factor;

    puntoPlayed *= factor;
    puntoWins *= factor;
    puntoLosses *= factor;

    return *this;
}
