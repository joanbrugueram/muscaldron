/*
    Copyright (C) 2012/2013 Joan Bruguera Micó.

    This file is part of MusCaldron.

    MusCaldron is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MusCaldron is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MusCaldron.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EXHAUSTIVE_H
#define EXHAUSTIVE_H

#include "hand.h"
#include <QFuture>

struct HandInfo
{
    const Hand *p1Hand;
    const Hand *p2Hand;
    HandFacts p234Facts[3];
};

class ExhaustiveSimulation
{
    std::vector<HandInfo> handList;

public:
    ExhaustiveSimulation(const Hand *p1Hand, HandFacts p234Facts[3]);
    QFuture<HandEvaluation> start();
};


#endif // EXHAUSTIVE_H
