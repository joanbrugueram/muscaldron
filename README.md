MusCaldron
==========
MusCaldron is a program which calculates the exact equities of [Mus](http://en.wikipedia.org/wiki/Mus_(card_game)) hands.

Doing the calculation naively would require evaluating 10,102,470,716,719,180,800 (ten quintillion, 44!/32!) different hands. Instead, MusCaldron uses combinatorial properties in order to group various card combinations into one hand, then does the computation using optimized code, making the calculation much quicker.

[PokerStove](http://www.pokerstove.com/) is a similar program for Poker hands.

Features
--------
- Mus variant supported: 8 reyes y 8 ases.
- Calculate the exact equity of any hand.
- Filter the hands of your team mate or the opposing team according to information obtained from [signs](http://en.wikipedia.org/wiki/Mus_(card_game\)#Signs).

Screenshot
----------
![MusCaldron Screenshot](https://bitbucket.org/joanbrugueram/muscaldron/raw/f4f93950cbe941ef94e353550670a84bf7e379af/Screenshot.png)

Compatiblity and licensing
--------------------------
MusCaldron has been tested under Windows and Linux.

MusCaldron is written in C++, using the Qt library. A Qt Creator project is included along the code.

MusCaldron is licensed under the [GNU GPL](http://www.gnu.org/licenses/gpl.html).

TODO List
---------
- Make sure that the results are right and exactly right (I'm not so sure about this!)
- Add unit tests.
- Filter the "perete" hand (4567).
- Optimize the program for more speed.
- Implement Mus a cuatro reyes (though it will likely require a lot more time).
