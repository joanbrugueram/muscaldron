/*
    Copyright (C) 2012/2013 Joan Bruguera Micó.

    This file is part of MusCaldron.

    MusCaldron is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MusCaldron is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MusCaldron.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFuture>
#include <QFutureWatcher>
#include "hand.h"
#include "exhaustive.h"
#include <memory>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);
    
private:
    void setGuiControlState(bool evaluating);

private slots:
    void on_startButton_clicked();
    void on_stopButton_clicked();

    void onEvaluationProgressChange(int progressValue);
    void onEvaluationFinished();

private:
    Ui::MainWindow *ui;
    std::auto_ptr<ExhaustiveSimulation> esim;
    QFuture<HandEvaluation> future;
    QFutureWatcher<HandEvaluation> watcher;
};

#endif // MAINWINDOW_H
