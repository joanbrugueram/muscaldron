/*
    Copyright (C) 2012/2013 Joan Bruguera Micó.

    This file is part of MusCaldron.

    MusCaldron is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MusCaldron is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MusCaldron.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "exhaustive.h"
#include <QtConcurrentMap>

ExhaustiveSimulation::ExhaustiveSimulation(const Hand *p1Hand, HandFacts p234Facts[3])
{
    /* Create hands for the first two players (that's 715 hands)
     * The map function will then make the hands of the rest of the players,
     * run the simulation, then the reduce function will combine the results. */
    int cards[4];
    for (cards[0] = 0; cards[0] < 10; cards[0]++)
    {
        for (cards[1] = cards[0]; cards[1] < 10; cards[1]++)
        {
            for (cards[2] = cards[1]; cards[2] < 10; cards[2]++)
            {
                for (cards[3] = cards[2]; cards[3] < 10; cards[3]++)
                {
                    const Hand *p2Hand = Hand::getHand(cards);
                    if (!p2Hand->compliesWith(p234Facts[0]))
                        continue;

                    HandInfo info;
                    info.p1Hand = p1Hand;
                    info.p2Hand = p2Hand;
                    std::copy(p234Facts, p234Facts + 3, info.p234Facts);
                    handList.push_back(info);
                }
            }
        }
    }
}

HandEvaluation mapTest(const HandInfo &info)
{
    HandEvaluation ev;

    const Hand *hands[4];
    hands[0] = info.p1Hand;
    hands[1] = info.p2Hand;

    int deckOdds[10] = { 8,4,4,4,4, 4,4,4,4,8 };
    for (int i = 0; i < 4; i++)
        deckOdds[hands[0]->cards[i]]--;

    uint64_t handOdds = hands[1]->equivalenceClass;
    for (int i = 0; i < 4; i++)
        handOdds *= deckOdds[hands[1]->cards[i]]--;

    int p3Cards[4], p4Cards[4];
    for (p3Cards[0] = 0; p3Cards[0] < 10; p3Cards[0]++)
    {
        uint64_t odds1 = handOdds * deckOdds[p3Cards[0]]--;
        for (p3Cards[1] = p3Cards[0]; p3Cards[1] < 10; p3Cards[1]++)
        {
            uint64_t odds2 = odds1 * deckOdds[p3Cards[1]]--;
            for (p3Cards[2] = p3Cards[1]; p3Cards[2] < 10; p3Cards[2]++)
            {
                uint64_t odds3 = odds2 * deckOdds[p3Cards[2]]--;
                for (p3Cards[3] = p3Cards[2]; p3Cards[3] < 10; p3Cards[3]++)
                {
                    hands[2] = Hand::getHand(p3Cards);
                    if (!hands[2]->compliesWith(info.p234Facts[1]))
                        continue;

                    uint64_t odds4 = odds3 * deckOdds[p3Cards[3]]--;
                    odds4 *= hands[2]->equivalenceClass;
                    for (p4Cards[0] = 0; p4Cards[0] < 10; p4Cards[0]++)
                    {
                        uint64_t odds5 = odds4 * deckOdds[p4Cards[0]]--;
                        for (p4Cards[1] = p4Cards[0]; p4Cards[1] < 10; p4Cards[1]++)
                        {
                            uint64_t odds6 = odds5 * deckOdds[p4Cards[1]]--;
                            for (p4Cards[2] = p4Cards[1]; p4Cards[2] < 10; p4Cards[2]++)
                            {
                                uint64_t odds7 = odds6 * deckOdds[p4Cards[2]]--;
                                for (p4Cards[3] = p4Cards[2]; p4Cards[3] < 10; p4Cards[3]++)
                                {
                                    hands[3] = Hand::getHand(p4Cards);
                                    if (!hands[3]->compliesWith(info.p234Facts[2]))
                                        continue;

                                    uint64_t odds8 = odds7 * deckOdds[p4Cards[3]]--;
                                    odds8 *= hands[3]->equivalenceClass;

                                    HandEvaluation r;
                                    r.addHandShowdown(hands);
                                    r *= odds8;
                                    ev += r;

                                    deckOdds[p4Cards[3]]++;
                                }
                                deckOdds[p4Cards[2]]++;
                            }
                            deckOdds[p4Cards[1]]++;
                        }
                        deckOdds[p4Cards[0]]++;
                    }
                    deckOdds[p3Cards[3]]++;
                }
                deckOdds[p3Cards[2]]++;
            }
            deckOdds[p3Cards[1]]++;
        }
        deckOdds[p3Cards[0]]++;
    }

    return ev;
}

void reduceTest(HandEvaluation &res, const HandEvaluation &tmp)
{
    res += tmp;
}

QFuture<HandEvaluation> ExhaustiveSimulation::start()
{
    return QtConcurrent::mappedReduced(handList, mapTest, reduceTest);
}
