#-------------------------------------------------
#
# Project created by QtCreator 2012-04-01T16:56:54
#
#-------------------------------------------------

QT       += core gui

TARGET = muscaldron
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    hand.cpp \
    exhaustive.cpp

HEADERS  += mainwindow.h \
    hand.h \
    exhaustive.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    COPYING.txt \
    TODO.txt
