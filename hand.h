/*
    Copyright (C) 2012/2013 Joan Bruguera Micó.

    This file is part of MusCaldron.

    MusCaldron is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MusCaldron is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MusCaldron.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef PLAYERINFO_H
#define PLAYERINFO_H

#include <string>

typedef unsigned long long uint64_t;

enum CardValue
{
    /* Integers assigned to each card value.
     * Those are designed in order to make comparison easier.
     *
     * Note that in the Mus variant simulated by this program,
     * deuces are equivalent to aces, and threes are equivalent to kings. */
    CardValue_Ace = 0, // "As"
    CardValue_2 = 0,
    CardValue_3 = 9,
    CardValue_4 = 1,
    CardValue_5 = 2,
    CardValue_6 = 3,
    CardValue_7 = 4,
    CardValue_8 = 5,
    CardValue_9 = 6,
    CardValue_Jack = 7, // "Sota"
    CardValue_Knight = 8, // "Caballo"
    CardValue_King = 9 // "Rey"
};

struct HandFacts
{
    /* Has exactly 2 kings/threes */
    bool has2K;
    /* Has exactly 2 aces/deuces */
    bool has2A;
    /* Has exactly 3 kings/threes */
    bool has3K;
    /* Has exactly 3 aces/deuces */
    bool has3A;
    /* Has exactly three equal cards */
    bool hasTrips;
    /* Has two pairs */
    bool hasTwoPair;
    /* Has no pairs and no juego */
    bool hasNothing;
    /* The juego sum is exactly 30. */
    bool sums30;
    /* The juego sum is exactly 31. */
    bool sums31;
};

struct Hand
{
    /* The cards that make up the hand, according to the values above. */
    int cards[4];
    /* The number of hands which contain the same cards in a different order. */
    int equivalenceClass;
    /* An integer value such that if hand1.grandeValue > hand2.grandeValue,
     * then hand1 wins hand2 at the grande (same for draws and losses). */
    int grandeValue;
    /* An integer value such that if hand1.chicaValue > hand2.chicaValue,
     * then hand1 wins hand2 at the chica (same for draws and losses). */
    int chicaValue;
    /* An integer value such that if hand1.paresValue > hand2.paresValue,
     * then hand1 wins hand2 at the pares (same for draws and losses). */
    int paresValue;
    /* The juego/punto sum of the card values. */
    int juegoSum;
    /* A value which is similar to the normal juego/punto sum of the cards,
     * except that for values bigger than 31 normal comparison works
     * (no weird rules for deciding which juego is better).
     *
     * This value can be used for both juego and punto comparisons. */
    int juegoValue;
    /* Facts about this hand. */
    HandFacts facts;


private:
    Hand(const int cards[4]);

public:
    static const Hand *getHand(const int cards[4]);
    static const Hand *fromString(const std::string &cardsString);
    std::string toString(void) const;
    bool compliesWith(const HandFacts &requisites) const;
};

struct HandEvaluation
{
    // WARNING! Be very careful when working with those values,
    // because they may have values close to the range limit
    // (2^63 < 44!/32! < 2^64)

    uint64_t gamesSimulated;

    uint64_t gamesPlayed;
    uint64_t grandeWins;
    uint64_t grandeLosses;
    uint64_t chicaWins;
    uint64_t chicaLosses;

    uint64_t paresPlayed;
    uint64_t paresWins;
    uint64_t paresLosses;

    uint64_t juegoPlayed;
    uint64_t juegoWins;
    uint64_t juegoLosses;

    uint64_t puntoPlayed;
    uint64_t puntoWins;
    uint64_t puntoLosses;

    HandEvaluation();
    void addHandShowdown(const Hand *hands[4]);
    HandEvaluation& operator +=(const HandEvaluation &other);
    HandEvaluation& operator *=(const uint64_t factor);
};


#endif // PLAYERINFO_H
